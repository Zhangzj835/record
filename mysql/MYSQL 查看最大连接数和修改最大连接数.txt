MySQL查看最大连接数和修改最大连接数

1、查看最大连接数
show variables like '%max_connections%';
2、修改最大连接数
set GLOBAL max_connections = 200;

  以下的文章主要是向大家介绍的是MySQL最大连接数的修改，我们大家都知道MySQL最大连接数的默认值是100, 这个数值对于并发连接很多的数据库的应用是远不够用的，当连接请求大于默认连接数后，就会出现无法连接数据库的错误，因此我们需要把它适当调大一些。在使 用MySQL数据库的时候，经常会遇到这么一个问题，就是“Can not connect to MySQL server. Too many connections”-mysql 1040错误，这是因为访问MySQL且还未释放的连接数目已经达到MySQL的上限。通常，mysql的最大连接数默认是100, 最大可以达到16384。

    常用的修改最大连接数的最常用的两种方式如下:

    第一种：命令行查看和修改最大连接数(max_connections)。

  >mysql -uuser -ppassword(命令行登录MySQL)

    mysql>show variables like 'max_connections';(查可以看当前的最大连接数)
    msyql>set global max_connections=1000;(设置最大连接数为1000，可以再次查看是否设置成功)
    mysql>exit  
    这种方式有个问题，就是设置的最大连接数只在mysql当前服务进程有效，一旦mysql重启，又会恢复到初始状态。因为mysql启动后的初始化工作是从其配置文件中读取数据的，而这种方式没有对其配置文件做更改。

    第二种：通过修改配置文件来修改mysql最大连接数(max_connections)。

    这种方式说来很简单，只要修改MySQL配置文件my.ini 或 my.cnf的参数max_connections，将其改为max_connections=1000，然后重启MySQL即可。但是有一点最难的就是 my.ini这个文件在哪找。通常有两种可能，一个是在安装目录下，另一种是在数据文件的目录下，安装的时候如果没有人为改变目录的话，一般就在 C:/ProgramData/MySQL往下的目录下,linux系统中一般在/etc目录下。

  其他需注意的：

   在编程时，由于用MySQL语句调用数据库时，在每次之执行语句前，会做一个临时的变量用来打开数据库，所以你在使用MySQL语句的时候，记得在每次调用完MySQL之后就关闭MySQL临时变量。

    另外对于访问量大的，可以考虑直接写到文本中，根据预测的访问量，先定义假若是100个文件文件名,需要的时候，再对所有文本文件中的数据进行分析，再导入数据库。 

 

如果是root帐号，你能看到所有用户的当前连接。如果是其它普通帐号，只能看到自己占用的连接

怎么进入mysql命令行呢？ mysql的安装目录下面有个bin目录，先用命令行进入该目录，然后用 mysql -uroot -p123456 来登录（注意：用户名和密码不用包含“”）


命令： show processlist; 
如果是root帐号，你能看到所有用户的当前连接。如果是其它普通帐号，只能看到自己占用的连接。


show processlist;只列出前100条，如果想全列出请使用show full processlist; 
mysql> show processlist;


命令： show status;
命令：show status like '%下面变量%'; 
Aborted_clients                由于客户没有正确关闭连接已经死掉，已经放弃的连接数量。 
Aborted_connects            尝试已经失败的MySQL服务器的连接的次数。 
Connections                     试图连接MySQL服务器的次数。 
Created_tmp_tables          当执行语句时，已经被创造了的隐含临时表的数量。 
Delayed_insert_threads     正在使用的延迟插入处理器线程的数量。 
Delayed_writes                用INSERT DELAYED写入的行数。 
Delayed_errors                用INSERT DELAYED写入的发生某些错误(可能重复键值)的行数。 
Flush_commands            执行FLUSH命令的次数。 
Handler_delete                 请求从一张表中删除行的次数。 
Handler_read_first          请求读入表中第一行的次数。 
Handler_read_key           请求数字基于键读行。 
Handler_read_next           请求读入基于一个键的一行的次数。 
Handler_read_rnd           请求读入基于一个固定位置的一行的次数。 
Handler_update              请求更新表中一行的次数。 
Handler_write                 请求向表中插入一行的次数。 
Key_blocks_used            用于关键字缓存的块的数量。 
Key_read_requests         请求从缓存读入一个键值的次数。 
Key_reads                      从磁盘物理读入一个键值的次数。 
Key_write_requests         请求将一个关键字块写入缓存次数。 
Key_writes                      将一个键值块物理写入磁盘的次数。 
Max_used_connections    同时使用的连接的最大数目。 
Not_flushed_key_blocks   在键缓存中已经改变但是还没被清空到磁盘上的键块。 
Not_flushed_delayed_rows      在INSERT DELAY队列中等待写入的行的数量。 
Open_tables                  打开表的数量。 
Open_files                     打开文件的数量。 
Open_streams               打开流的数量(主要用于日志记载） 
Opened_tables              已经打开的表的数量。 
Questions                     发往服务器的查询的数量。 
Slow_queries                要花超过long_query_time时间的查询数量。 
Threads_connected       当前打开的连接的数量。 
Threads_running          不在睡眠的线程数量。 
Uptime                        服务器工作了多少秒。